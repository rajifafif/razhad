const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js')

	//.sass('resources/assets/sass/app.scss', 'public/css')''
	
mix.js('resources/assets/js/app.js', 'public/js/app.js')

	//core
	.scripts('resources/assets/remark/vendor/breakpoints/breakpoints.js','public/js/vendor/breakpoints/breakpoints.js')
	.scripts('resources/assets/remark/vendor/babel-external-helpers/babel-external-helpers.js','public/js/vendor/babel-external-helpers/babel-external-helpers.js')
	.scripts('resources/assets/remark/vendor/jquery/jquery.js','public/js/vendor/jquery/jquery.js')
	.scripts('resources/assets/remark/vendor/tether/tether.js','public/js/vendor/tether/tether.js')
	.scripts('resources/assets/remark/vendor/bootstrap/bootstrap.js','public/js/vendor/bootstrap/bootstrap.js')
	.scripts('resources/assets/remark/vendor/animsition/animsition.js','public/js/vendor/animsition/animsition.js')
	.scripts('resources/assets/remark/vendor/mousewheel/jquery.mousewheel.js','public/js/vendor/mousewheel/jquery.mousewheel.js')
	.scripts('resources/assets/remark/vendor/asscrollbar/jquery-asScrollbar.js','public/js/vendor/asscrollbar/jquery-asScrollbar.js')
	.scripts('resources/assets/remark/vendor/asscrollable/jquery-asScrollable.js','public/js/vendor/asscrollable/jquery-asScrollable.js')
	.scripts('resources/assets/remark/vendor/ashoverscroll/jquery-asHoverScroll.js','public/js/vendor/ashoverscroll/jquery-asHoverScroll.js')
	//plugin
	.scripts('resources/assets/remark/vendor/switchery/switchery.min.js','public/js/vendor/switchery/switchery.min.js')
	.scripts('resources/assets/remark/vendor/intro-js/intro.js','public/js/vendor/intro-js/intro.js')
	.scripts('resources/assets/remark/vendor/screenfull/screenfull.js','public/js/vendor/screenfull/screenfull.js')
	.scripts('resources/assets/remark/vendor/slidepanel/jquery-slidePanel.js','public/js/vendor/slidepanel/jquery-slidePanel.js')
	.scripts('resources/assets/remark/vendor/multi-select/jquery.multi-select.js','public/js/vendor/multi-select/jquerymulti-select.js')
	.styles([
	    'resources/assets/remark/css/bootstrap.css',
	    'resources/assets/remark/css/bootstrap-extend.css',
	    'resources/assets/remark/css/base-site.min.css',

	    //fonts
	    // 'resources/assets/remark/fonts/web-icons/web-icons.min.css',
	    // 'resources/assets/remark/fonts/brand-icons/brand-icons.min.css'

	    //plugins
	    'resources/assets/remark/vendor/animsition/animsition.css',
	    'resources/assets/remark/vendor/asscrollable/asScrollable.css',
	    'resources/assets/remark/vendor/switchery/switchery.css',
	    'resources/assets/remark/vendor/intro-js/introjs.css',
	    'resources/assets/remark/vendor/slidepanel/slidePanel.css',
	    // 'resources/assets/remark/vendor/flag-icon-css/flag-icon.css',
	], 'public/css/all.css')
	.styles('resources/assets/remark/vendor/multi-select/multi-select.min.css','public/css/multi-select.min.css')
	.sass('resources/assets/sass/app.scss', 'public/css');
