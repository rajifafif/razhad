<?php $active_nav = 'product-add'; ?>

@extends('backend.layout')

@section('active_product-list', 'm-menu__item--active')

@section('title', 'Add New Product')

@section('breadcumb')

<li class="m-nav__separator">
	-
</li>
<li class="m-nav__item">
	<a href="" class="m-nav__link">
		<span class="m-nav__link-text">
			Product
		</span>
	</a>
</li>
<li class="m-nav__separator">
	-
</li>
<li class="m-nav__item">
	<a href="" class="m-nav__link">
		<span class="m-nav__link-text">
			@yield('title')
		</span>
	</a>
</li>
@endsection

@section('content')
	<div class="m-portlet">
		<!--begin::Form-->
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="form" novalidate="novalidate" method="POST" action="{{	route('attribute.store') }}">
			{{ csrf_field() }}
			
			@include('backend.attribute._form')
		</form>
		<!--end::Form-->
	</div>
@endsection