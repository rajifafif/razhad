<?php $active_nav = 'product-list'; ?>

@extends('backend.layout')

@section('active_product-list', 'm-menu__item--active')

@section('title', 'Product Lists')

@section('content')
	<div class="m-portlet m-portlet--tab">
		<div id="m_datatable"></div>
		
		
	</div>
@endsection

@section('scripts')
<script type="text/javascript">

var options = {
    data: {
        type: 'local',
        source: [{'OrderID':1,'ShipCountry':'ID'},{'OrderID':1,'ShipCountry':'ID'},{'OrderID':1,'ShipCountry':'ID'},{'OrderID':1,'ShipCountry':'ID'}],
        pageSize: 10,
        saveState: {
            cookie: true,
            webstorage: true
        },

        serverPaging: false,
        serverFiltering: false,
        serverSorting: false
    },
    // columns definition
    columns: [{
        field: "RecordID",
        title: "#",
        locked: {left: 'xl'},
        sortable: false,
        width: 40,
        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
    }, {
        field: "OrderID",
        title: "Order ID",
        sortable: 'asc',
        filterable: false,
        width: 150,
        responsive: {visible: 'lg'},
        locked: {left: 'xl'},
        template: '@{{OrderID}} - @{{ShipCountry}}'
    }, {
        field: "ShipCountry",
        title: "Ship Country",
        width: 150,
        overflow: 'visible',
        template: function (row) {
            return row.ShipCountry + ' - ' + row.ShipCity;
        }
    }]
}
var datatable = $('#m_datatable').mDatatable(options);
</script>

@endsection