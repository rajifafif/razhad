<?php $active_nav = 'product-list'; ?>

@extends('backend.layout')

@section('active_product-list', 'm-menu__item--active')

@section('title', 'Product Lists')

@section('content')
	<div class="m-portlet m-portlet--tab">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Base Form Controls
												</h3>
											</div>
										</div>
									</div>
									<!--begin::Form-->
									<form class="m-form m-form--fit m-form--label-align-right" method="GET">
										<div class="m-portlet__body">
											<div class="form-group m-form__group m--margin-top-10">
												<div class="alert m-alert m-alert--default" role="alert">
													The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
												</div>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													Product Name
												</label>
												<input type="text" name="nama_produk" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Nama Produk">
												<span class="m-form__help">
													We'll never share your email with anyone else.
												</span>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleTextarea">
													Short Description
												</label>
												<textarea class="form-control m-input" name="short_description" id="exampleTextarea" rows="3"></textarea>
											</div>

											<div class="form-group m-form__group">
												<label for="exampleTextarea">
													Description
												</label>
												<textarea class="form-control m-input"  name="description" id="exampleTextarea" rows="3"></textarea>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													SKU
												</label>
												<input type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Nama Produk">
												<span class="m-form__help">
													We'll never share your email with anyone else.
												</span>
											</div>
											<div class="form-group m-form__group">
												<label for="exampleSelect1">
													Type
												</label>
												<select class="form-control m-input" id="exampleSelect1">
													<option>
														1
													</option>
													<option>
														2
													</option>
													<option>
														3
													</option>
													<option>
														4
													</option>
													<option>
														5
													</option>
												</select>
											</div>

											<div class="form-group m-form__group">
												<label for="exampleInputEmail1">
													Stock
												</label>
												<input type="text" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Nama Produk">
												<span class="m-form__help">
													We'll never share your email with anyone else.
												</span>
											</div>
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="submit" class="btn btn-primary">
													Submit
												</button>
												<button type="reset" class="btn btn-secondary">
													Cancel
												</button>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
@endsection