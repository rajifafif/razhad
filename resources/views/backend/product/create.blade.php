<?php $active_nav = 'product-add';?>
@extends('backend.layout')

@section('active_product-list', 'm-menu__item--active')

@section('title', 'Add New Product')

@section('breadcumb')
<li class="m-nav__separator">
	-
</li>
<li class="m-nav__item">
	<a class="m-nav__link" href="">
		<span class="m-nav__link-text">
			Product
		</span>
	</a>
</li>
<li class="m-nav__separator">
	-
</li>
<li class="m-nav__item">
	<a class="m-nav__link" href="">
		<span class="m-nav__link-text">
			@yield('title')
		</span>
	</a>
</li>
@endsection

@section('content')
<div id="wrapper">
<form action="{{ route('product.create') }}">
{{ csrf_field() }}
<div class="panel panel-bordered">
	<div class="panel-heading">
		<h2 class="panel-title">
			Product Basic Information
		</h2>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="form-control-label" for="name">
				Product Name
			</label>
			<input class="form-control" id="name" name="name" placeholder="ex: Red T-Shirt" type="text"/>
		</div>
		<div class="form-group">
			<label class="form-control-label" for="sku">
				SKU
			</label>
			<input class="form-control" id="sku" name="sku" placeholder="ex: TSRED01" type="text"/>
		</div>
		<div class="form-group">
			<label class="form-control-label" for="short_description">
				Short Description
			</label>
			<textarea class="form-control short_description" id="short_description" name="short_description"></textarea>
			<div class="text-help">
				This description will appear beside the product image
			</div>
		</div>
		<div class="form-group">
			<label class="form-control-label" for="description">
				Full Description
			</label>
			<textarea class="form-control description" id="description" name="description"></textarea>
			<div class="text-help">
				This description will appear on Product Description section
			</div>
		</div>
		<div class="form-group">
			<label class="form-control-label" for="stock">
				Stock
			</label>
			<input class="form-control" id="stock" min="0" name="stock" placeholder="ex: 100" type="number"/>
		</div>
		<div class="form-group">
			<label class="form-control-label" for="weight">
				Weight (kg)
			</label>
			<input class="form-control" id="weight" min="0" name="weight" placeholder="ex: 1" type="number"/>
		</div>
		<div class="form-group">
			<label class="form-control-label" for="dimmension">
				Dimmension (cm) (Width x Long x Height)
			</label>
			<div class="row">
				<div class="col-lg-4">
					<input class="form-control" id="width" min="0" name="width" placeholder="Width" type="number"/>
				</div>
				<div class="col-lg-4">
					<input class="form-control" id="long" min="0" name="long" placeholder="Long" type="number"/>
				</div>
				<div class="col-lg-4">
					<input class="form-control" id="height" min="0" name="height" placeholder="Height" type="number"/>
				</div>
			</div>
		</div>
	</div>
</div>
</form> <!-- END FORM -->
<div class="panel panel-bordered">
	<div class="panel-heading">
		<h2 class="panel-title">
			Product Options
		</h2>
		<div class="panel-actions">
			<form class="form-inline">
				<div class="form-group">
					<label class="form-control-label" for="type">
						Product Type :
					</label>
					<select class="form-control" id="type" name="type" v-model="type">
						<option value="simple">
							Simple Product
						</option>
						<option value="variable">
							Variabel Product
						</option>
						<option value="grouped">
							Grouped Product
						</option>
					</select>
				</div>
			</form>
		</div>
	</div>
	<div class="panel-body">
		<div class="nav-tabs-vertical" data-plugin="tabs">
			<ul class="nav nav-tabs mr-25" role="tablist">
				<li class="nav-item" role="presentation">
					<a aria-controls="atributesTab" class="nav-link active" data-toggle="tab" href="#atributesTab" role="tab">
						Attributes
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a aria-controls="variationTab" class="nav-link" data-toggle="tab" href="#variationTab" role="tab">
						Variation
					</a>
				</li>
			</ul>
			<div class="tab-content py-15">
				<div class="tab-pane active" id="atributesTab" role="tabpanel">

					<!-- ATTRIBUTE GROUP -->
					<div class="form-group row">
						<label class="col-lg-3 col-form-label" for="example-text-input">
							Add Attribute Group
						</label>
						<div class="col-lg">
							<select class="form-control" id="attribute_group_id" name="attribute_group_id" v-model="attribute_group_id">
								<option value="">
								</option>
								<option v-for="attributeGroup in attributeGroups" :value="attributeGroup.id"> 
									@{{ attributeGroup.name }} (<template v-for="attribute in attributeGroup.attributes">@{{ attribute.name }},</template>)
								</option>

							</select>
						</div>
						<div class="col-lg-1">
							<button class="button form-control" href="#asdasd" id="add-attribute_group">
								Add
							</button>
						</div>
					</div>


					<!-- CUSTOM ATTRIBUTE -->
					<div class="form-group row">
						<label class="col-lg-3 col-form-label" for="example-text-input">
							Add Custom Attribute
						</label>
						<div class="col-lg">
							<select class="form-control" id="attribute_id" name="attribute_id" v-model="attribute_id">
								<option value="">
								</option>
								<option v-for="attribute in attributes" :value="attribute.id"> @{{ attribute.name }}</option>
								
							</select>
						</div>
						<div class="col-lg-1">
							<button class="button form-control" id="add-attribute" v-on:click="addAttribute()">
								Add
							</button>
						</div>
					</div>

						your attributes : @{{ product_attributes }}
					<div aria-multiselectable="true" class="panel-group panel-group-continuous" id="attributesList" role="tablist" style="border: solid 1px #e4eaec;">
						<div class="panel" v-for="product_attribute in product_attributes">
							<div class="panel-heading" id="attribute-ID" role="tab">
								<a aria-controls="attribute-ID" aria-expanded="false" class="panel-title" data-parent="#attributesList" data-toggle="collapse" href="#exampleCollapseOne">
									@{{product_attribute.name}}
								</a>
							</div>
							<div aria-labelledby="attribute-ID" class="panel-collapse collapse" id="exampleCollapseOne" role="tabpanel" style="">
								<div class="panel-body">
									<div class="form-group row">
										<div class="col-lg-3">
											<input class="form-control" disabled="" name="" type="text" :value="product_attribute.name">
											</input>
											<br>
												<div class="checkbox-custom checkbox-primary">
													<input id="attribute-ID-variation" name="attribute-ID-variation" type="checkbox" v-model="product_attribute.variation">
														<label for="attribute-ID-variation">
															Use for Variation
														</label>
													</input>
												</div>
											</br>
										</div>
										<div class="col-lg">
											<textarea class="form-control" placeholder='Use "|" as separator' rows="4" v-model="product_attribute.options"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="panel">
							<div class="panel-heading" id="attribute-ID2" role="tab">
								<a aria-controls="exampleCollapseOne" aria-expanded="false" class="panel-title" data-parent="#attributesList" data-toggle="collapse" href="#exampleCollapseTwo">
									Attribute Name
								</a>
							</div>
							<div aria-labelledby="attribute-ID2" class="panel-collapse collapse" id="exampleCollapseTwo" role="tabpanel" style="">
								<div class="panel-body">
									<div class="form-group row">
										<div class="col-3">
											<input class="form-control" disabled="" name="" type="text" value="Attribute Name">
											</input>
											<br>
												<div class="checkbox-custom checkbox-primary">
													<input id="attribute-ID-variation" name="attribute-ID-variation" type="checkbox">
														<label for="attribute-ID-variation">
															Use for Variation
														</label>
													</input>
												</div>
											</br>
										</div>
										<div class="col-lg">
											<textarea class="form-control" placeholder='Use "|" as separator' rows="4"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button class="btn btn-primary">
						Save Attributes
					</button>
				</div>
				<div class="tab-pane" id="variationTab" role="tabpanel">
					Quaerat delectus arte exhorrescere summum disputando agatur perfunctio, e videntur
	repellere jhasdkjhasdkjhasdkjhqweiuoqweoihqwiouehqwoehuqwheiuqwheiuhqweuihFingitur iudico simulent polyaeno conclusionemque
	atomis placatae solido etiam, optinere defenditur libero
	consequentis aristoteli scribentur curis iudicium divinum.
	Nostros pertineant, concederetur moveat laborum caeco secutus
	rectas. Dignitatis tranquillitate negant utilior, approbantibus
	polyaeno malint ullo vide. Possum sane confidam cogitavisse.
	Sumitur. Diis.
				</div>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-bordered">
	<div class="panel-heading">
		<h2 class="panel-title">
			Linked Products
		</h2>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="form-control-label" for="type">
				Up Sell
			</label>
			<select class="form-control" id="type" name="type" v-model="type">
				<option value="simple">
					Simple Product
				</option>
				<option value="variable">
					Variabel Product
				</option>
				<option value="grouped">
					Grouped Product
				</option>
			</select>
		</div>
		<div class="form-group">
			<label class="form-control-label" for="type">
				Cross Sell
			</label>
			<select class="form-control" id="type" name="type" v-model="type">
				<option value="simple">
					Simple Product
				</option>
				<option value="variable">
					Variabel Product
				</option>
				<option value="grouped">
					Grouped Product
				</option>
			</select>
		</div>
	</div>
</div>
<div>
	<button class="btn btn-primary" type="submit">
		Submit
	</button>
	<button class="btn btn-secondary" type="reset">
		Cancel
	</button>
</div>
</div><!-- END WRAPPER -->
@endsection


@section('scripts')

<script type="text/javascript">


var data = {
	attributeGroups : <?= $attributeGroups; ?>,
	attributes : <?= $attributes; ?>,

	attribute_group_id : '',
	attribute_id : '',
	type : '',
	product_attributes : []
}
var vue = new Vue({
	el: '#wrapper',
	data: data,
	methods: {
	    addAttribute: function (event) {
			if(attribute_id.value != ""){
				var addedAttr = vue.attributes[attribute_id.value];
				addedAttr.variation = true;
				addedAttr.options = "";

				vue.product_attributes.push(addedAttr);
			}
		}
	}
});
console.log(vue.data);
// console.log(app.data);

</script>
<!-- <script type="text/javascript">
	//== Class definition
var FormControls = function () {
	//== Private functions

	var formValidation = function () {

	}
	return {
		// public functions
		init: function() {
			formValidation();
		}
	};
}();


jQuery(document).ready(function() {
	$( "" ).validate({
		// define validation rules
		rules: {
			name: {
				required: true
			},
			sku: {
				required: true
			},
			short_description: {
				required: true
			},
			description: {
				required: true
			},
			width: {
				digits: true
			},
			long: {
				digits: true
			},
			height: {
				digits: true
			},
			attribute_group_id:{
				required: true
			}
		},

		//display error alert on form submit
		invalidHandler: function(event, validator) {
			var alert = $('#form-alert');
			alert.removeClass('m--hide').show();
			mApp.scrollTo(alert, -200);
		},

		submitHandler: function (form) {
			form[0].submit(); // submit the form
		}
	});

	$('.short_description').summernote({
		height: 130,
	});
	$('.description').summernote({
		height: 200,
	});

});
</script> -->
@endsection
