<div class="m-portlet__body">

	<br>

	<div class="m-form__content">
		<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="form-alert">
			<div class="m-alert__icon">
				<i class="la la-warning"></i>
			</div>
			<div class="m-alert__text">
				Oh snap! Check a few things up and try submitting again.
			</div>
			<div class="m-alert__close">
				<button type="button" class="close" data-close="alert" aria-label="Close"></button>
			</div>
		</div>
	</div>

	<div class="form-group m-form__group">
		<label for="name">
			Product Name
		</label>
		<input type="text" name="name" id="name" class="form-control m-input" placeholder="ex: Red T-Shirt">
	</div>

	<div class="form-group m-form__group">
		<label for="sku">
			SKU
		</label>
		<input type="text" class="form-control m-input" name="sku" id="sku" placeholder="ex: TSRED01">
	</div>

	<div class="form-group m-form__group">
		<label for="short_description">
			Short Description
		</label>
		<div class="m-form__help">
			This description will appear beside the product image
		</div>
		<textarea class="form-control m-input short_description" name="short_description" id="short_description"></textarea>
		
	</div>

	<div class="form-group m-form__group">
		<label for="description">
			Full Description
		</label>
		<div class="m-form__help">
			This description will appear on Product Description section
		</div>
		<textarea class="form-control m-input description"  name="description" id="description"></textarea>
	</div>

	<div class="form-group m-form__group">
		<label for="stock">
			Stock
		</label>
		<input type="number" class="form-control m-input" name="stock" id="stock" placeholder="ex: 100" min="0">
	</div>

	<div class="form-group m-form__group">
		<label for="weight">
			Weight (kg)
		</label>
		<input type="number" class="form-control m-input" name="weight" id="weight" placeholder="ex: 1" min="0">
	</div>

	<div class="form-group m-form__group">

		<label for="dimmension">
			Dimmension (cm) (Width x Long x Height)
		</label>
		<div class="row">
			<div class="col-lg-4">
				<input type="number" class="form-control m-input" name="width" id="width" placeholder="Width" min="0">
			</div>
			<div class="col-lg-4">
				<input type="number" class="form-control m-input" name="long" id="long" placeholder="Long" min="0">
			</div>
			<div class="col-lg-4">
				<input type="number" class="form-control m-input" name="height" id="height" placeholder="Height" min="0">
			</div>
		</div>
	</div>

</div>

<div>
	
	<div class="form-group m-form__group">
		<label for="weight">
			Attribute Group
		</label>
		<select class="form-control m-select2" id="attribute_group_id" name="attribute_group_id">
			<option value="">Select Attribute Group</option>
		</select>


	</div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
	<div class="m-form__actions">
		<button type="submit" class="btn btn-primary">
			Submitss
		</button>
		<button type="reset" class="btn btn-secondary">
			Cancel
		</button>
	</div>
</div>

@section('scripts')
<script type="text/javascript">
	//== Class definition

var FormControls = function () {
    //== Private functions
    
    var formValidation = function () {
        
    }
    return {
        // public functions
        init: function() {
            formValidation(); 
        }
    };
}();

jQuery(document).ready(function() {    
    $( "#form" ).validate({
        // define validation rules
        rules: {
            name: {
                required: true
            },
            sku: {
                required: true
            },
            short_description: {
                required: true 
            },
            description: {
                required: true 
            },
            width: {
                digits: true
            },
            long: {
                digits: true
            },
            height: {
                digits: true
            },
            attribute_group_id:{
            	required: true
            }
        },
        
        //display error alert on form submit  
        invalidHandler: function(event, validator) {     
            var alert = $('#form-alert');
            alert.removeClass('m--hide').show();
            mApp.scrollTo(alert, -200);
        },

        submitHandler: function (form) {
            form[0].submit(); // submit the form
        }
    });

    $('.short_description').summernote({
        height: 130, 
    });
    $('.description').summernote({
        height: 200, 
    });

    $('#attribute_group_id').select2();
});
</script>

@endsection