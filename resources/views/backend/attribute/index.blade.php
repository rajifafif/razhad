<?php $active_nav = 'product-list'; ?>

@extends('backend.layout')

@section('active_product-list', 'm-menu__item--active')

@section('title', 'Product Lists')

@section('content')
	<div class="m-portlet m-portlet--tab">

        <table class="table table-striped m-table m-table--head-bg-success">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>

            @foreach ($model as $attribute)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$attribute->name}}</td>
                    <td>{{$attribute->slug}}</td>
                    <td>{{$attribute->description}}</td>
                    <td style="overflow: visible; width: 120px;">
                        <!-- <a href="#" class="btn btn-sm btn-outline-success m-btn m-btn--icon m-btn--icon-only" title="View"><i class="fa fa-eye"></i></a> -->
                        <a href="{{ route('attribute.edit', ['id' => $attribute->id]) }}" class="btn btn-sm btn-outline-accent m-btn m-btn--icon m-btn--icon-only" title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only" title="Delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @endforeach
        </table>
	</div>
@endsection
