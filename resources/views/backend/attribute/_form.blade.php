<div class="m-portlet__body">

	<br>

	<div class="m-form__content">
		<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="form-alert">
			<div class="m-alert__icon">
				<i class="la la-warning"></i>
			</div>
			<div class="m-alert__text">
				Oh snap! Check a few things up and try submitting again.
			</div>
			<div class="m-alert__close">
				<button type="button" class="close" data-close="alert" aria-label="Close"></button>
			</div>
		</div>
	</div>

	@if ($errors->any())
	    <div class="m-form__content">
			<div class="m-alert m-alert--icon alert alert-danger" role="alert" id="form-alert">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					<ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	@endif

	<div class="form-group m-form__group">
		<label for="name">
			Name
		</label>
		<input type="text" name="name" id="name" class="form-control m-input" value="{{ old('name') }}">
	</div>

	<div class="form-group m-form__group">
		<label for="slug">
			Slug
		</label>
		<input type="text" name="slug" id="slug" class="form-control m-input" value="{{ old('slug') }}">
	</div>

	<div class="form-group m-form__group">
		<label for="type">
			Type
		</label>
		<select type="text" name="type" id="type" class="form-control m-input">
			<option value="text">Text</option>
			<option value="textarea">Textarea</option>
		</select>
	</div>

	<div class="form-group m-form__group">
		<label for="description">
			Description
		</label>
		<textarea name="description" id="description" class="form-control m-input" rows="4">{{ old('description') }}</textarea>
	</div>

</div>
<div class="m-portlet__foot m-portlet__foot--fit">
	<div class="m-form__actions">
		<button type="submit" class="btn btn-primary">
			Submit
		</button>
		<button type="reset" class="btn btn-secondary">
			Cancel
		</button>
	</div>
</div>

@section('scripts')
<script type="text/javascript">
	//== Class definition

var FormControls = function () {
    //== Private functions
    
    var formValidation = function () {
        
    }
    return {
        // public functions
        init: function() {
            formValidation(); 
        }
    };
}();

jQuery(document).ready(function() {    
    $( "#form" ).validate({
        // define validation rules
        rules: {
            name: {
                required: true
            },
        },
        
        //display error alert on form submit  
        invalidHandler: function(event, validator) {     
            var alert = $('#form-alert');
            alert.removeClass('m--hide').show();
            mApp.scrollTo(alert, -200);
        },

        submitHandler: function (form) {
            form[0].submit(); // submit the form
        }
    });
});
</script>

@endsection