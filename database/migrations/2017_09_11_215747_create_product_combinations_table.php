<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCombinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_combinations', function (Blueprint $table) {
            $table->integer('product_parent')->unsigned();
            $table->integer('product_child')->unsigned();
            $table->enum('type', ['variable', 'grouped','upsale','crosssale','recommendation']);


            $table->foreign('product_parent')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_child')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_combinations');
    }
}
