<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeGroupPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_group_pivots', function (Blueprint $table) {
            $table->integer('attribute_group_id')->unsigned();
            $table->integer('attribute_id')->unsigned();

            $table->primary(['attribute_group_id', 'attribute_id']);

            // $table->foreign('attribute_group_id')->references('id')->on('attribute_groups')->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('attribute_id')->references('id')->on('attributes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_group_pivots');
    }
}
