<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable(); 
            $table->integer('address_id')->unsigned()->nullable(); 

            $table->decimal('subtotal_payment', 10,2)->default(0);
            $table->string('coupon')->nullable();
            $table->decimal('discount', 10,2)->default(0);
            $table->decimal('shipping', 10,2)->default(0);
            $table->decimal('total_payment', 10,2)->default(0);

            $table->text('note'); 

            $table->enum('status', ['waiting_payment','received','on_process','done'])->default('waiting_payment');

            $table->timestamps();



            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
