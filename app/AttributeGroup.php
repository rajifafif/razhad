<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    //

    public $timestamps = false;

    public function attributes(){
    	return $this->belongsToMany('App\Attribute', 'attribute_group_pivots');
    }
}
