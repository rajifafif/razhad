<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    public function getYear(){
    	return substr($this->sku, 5,2);
    }

    public function attributes(){
    	return $this->belongsToMany('App\Attribute', 'product_attributes')->withPivot('value');
    }

    public function attributeGroup(){
    	return $this->hasOne('\App\AttributeGroup', 'id');	
    }
}
