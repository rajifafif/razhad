<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    //
    public $excluded_attributes = [2,3,4,5];

    public $timestamps = false;

    public function products(){
    	return $this->belongsToMany('App\Product', 'product_attributes');
    }

    public function groups(){
    	return $this->belongsToMany('App\AttributeGroup', 'attribute_group_pivots');
    }

    public function options(){
    	return $this->hasMany('App\AttributeOption', 'attribute_id');
    }
    public function productOptions($product_id){
        return $this->options->where('product_id', $product_id);
    }
    public function without($ids){
        return $this->whereNotIn('id', $ids)->get();
    }
}
