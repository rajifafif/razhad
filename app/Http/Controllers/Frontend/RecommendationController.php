<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\FPGrowth;

class RecommendationController extends Controller
{
    //

    public function index(){

    	$data = \App\Order::whereHas('orderproduct', function($query){
    		$query->where('product_id', 21);
    	})->with(['orderproduct' => function($query){
    		$query->orderBy('product_id', 'DESC');
    	}])->get();
        
        // $data = \App\Order::all();

    	// $data = DB::table('order')->pluck('id');
        //MAKE TABEL TRANSAKSI
        // echo "<table border='1' style='border-collapse:collapse'>";
        // echo "<tr><td>order_id</td><td colspan='4'>dataset</td></tr>";
        // foreach ($data as $transaction) {
        //     echo "<tr>";
        //         echo "<td><b>".$transaction->id."</b></td>";
        //         foreach ($transaction->orderproduct as $product) {
        //             echo "<td>";
        //             echo $product->product_id;
        //             echo "</td>";
        //         }
        //     echo "</tr>";
        // }
        // echo "</table>";

    	$transactions = [];

    	foreach ($data as $transaction) {

    		foreach ($transaction->orderproduct as $orderproduct) {
    			$transactions[$transaction->id][] = $orderproduct->product_id;
    		}
    		
    	}

    	echo "<pre>";

    	// print_r($transactions);

    	$FPGrowth = new FPGrowth($transactions);
        echo "transactions";
echo "<table border=1 border-collapse='collapse'>";
        foreach ($FPGrowth->transactions as $transaction_id => $products) {
            echo "<tr>
                <td>$transaction_id</td>
                <td>".implode(',',$products)."</td>
            </tr>";
        }
echo "</table>";

echo "<table border=1 border-collapse='collapse'>";
        foreach ($FPGrowth->itemordered as $item_id => $param) {
            echo "<tr>
                <td>$item_id</td>
                <td>".$param['count']."</td>
            </tr>";
        }
echo "</table>";
        
    
    echo "Recommendation";
        echo "<table border=1 border-collapse='collapse'>";
        foreach ($FPGrowth->getRecommended() as $number => $dataset) {
            echo "<tr>
                <td>".($number+1)."</td>
                <td>".implode(',',$dataset)."</td>
            </tr>";
        }
echo "</table>";

    	// print_r($FPGrowth->tree);
    }
}
