<?php

namespace App;

class FPGrowth{

	public $minimumSupport = 15; //percentage from max frequency
	public $minimumSupportCount;
	public $transactions = [];
	public $itemordered = [];
	public $tree = [];
	public $itemsets = [];



	public function __construct($data = []){
		$this->transactions = $data;

		//COUNT ITEM FREQUENCY AND ORDER
		$this->orderItem();
		$this->orderTransaction();

		$this->tree = $this->makeTree($this->transactions);

		$this->makeItemset($this->tree);
	}


	//order item by shown in transactions to show index & count times;
	private function orderItem(){
		//Count Transaction
		foreach ($this->transactions as $transaction) {
			foreach ($transaction as $item) {
				if (!isset($itemdata[$item])) {
					$itemdata[$item]['count'] = 1;
				}else{
					$itemdata[$item]['count']++;
				}
			}
		}

		//Sort Item by value
		arsort($itemdata);

		$index = 1;
		foreach ($itemdata as $item => $count) {
			$itemdata[$item]['index'] = $index;
			$index++;
		}

		$this->itemordered = $itemdata;

		foreach($itemdata as $first) {
			$this->minimumSupportCount = ceil($first['count'] * $this->minimumSupport / 100);
			break;
		}
	}

	//order item in transaction by itemordered
	private function orderTransaction(){
		foreach ($this->transactions as $id => $transaction) {

			$temp = [];
			foreach ($transaction as $item) {

				//REDUCE IF UNDER MINIMUM SUPPORT
				if ($this->itemordered[$item]['count'] >= $this->minimumSupportCount) {
					$temp[$item] = $this->itemordered[$item]['index'];
				}
			}

			asort($temp);

			$temp2 = [];
			foreach ($temp as $itemid => $index) {
				$temp2[] = $itemid;
			}
			$this->transactions[$id] = $temp2;
		}
	}



	//insert transaction to tree
	private function toTree(&$tree, $transaction){
		//order transaction by itemordered

		if (!empty($transaction)) {
			
			$item = $transaction[0];
			if (array_key_exists($item, $tree['branch'])) {
				$tree['branch'][$item]['count']++;
			}else{
				$newnode = ['count' => 1, 'branch' => []];
				$tree['branch'][$item] = $newnode;
			}

			array_shift($transaction);
			$this->toTree($tree['branch'][$item], $transaction);
		}
	}


	//generate tree by list of transaction (transaction's')
	private function makeTree($transactions){
		$tree  = ['count' => 'root', 'branch' => []];//root

		foreach ($transactions as $transaction) {
			$this->toTree($tree, $transaction);
		}

		return $tree;
	}

	public function makeItemset($tree, $itemset = []){
		if (!empty($tree['branch'])) {
			foreach ($tree['branch'] as $itemid => $property) {
				$tempitemset = $itemset;
				$tempitemset[] = $itemid;
				$this->makeItemset($tree['branch'][$itemid], $tempitemset);
			}
		}else{
			if (count($itemset) > 1) {
				$this->itemsets[] = $itemset;
			}
		}
	}

	public function getRecommended(){
		return $this->itemsets;
	}


}