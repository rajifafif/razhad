const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js')

	//.sass('resources/assets/sass/app.scss', 'public/css')''
	
mix.js('resources/assets/js/app.js', 'public/js/app.js')


  .scripts('resources/assets/remark/vendor/breakpoints/breakpoints.js','public/js/breakpoints.js')
  .scripts('resources/assets/remark/vendor/babel-external-helpers/babel-external-helpers.js','public/js/babel-external-helpers.js')
  .scripts('resources/assets/remark/vendor/jquery/jquery.js','public/js/jquery.js')
  .scripts('resources/assets/remark/vendor/tether/tether.js','public/js/tether.js')
  .scripts('resources/assets/remark/vendor/bootstrap/bootstrap.js','public/js/bootstrap.js')
  .scripts('resources/assets/remark/vendor/animsition/animsition.js','public/js/animsition.js')
  .scripts('resources/assets/remark/vendor/mousewheel/jquery.mousewheel.js','public/js/jquery.mousewheel.js')
  .scripts('resources/assets/remark/vendor/asscrollbar/jquery-asScrollbar.js','public/js/jquery-asScrollbar.js')
  .scripts('resources/assets/remark/vendor/asscrollable/jquery-asScrollable.js','public/js/jquery-asScrollable.js')
  .scripts('resources/assets/remark/vendor/ashoverscroll/jquery-asHoverScroll.js','public/js/jquery-asHoverScroll.js')
  .scripts('resources/assets/remark/vendor/slidepanel/jquery-slidePanel.js','public/js/jquery-slidePanel.js')
	.styles([
	    'resources/assets/remark/css/bootstrap.css',
	    'resources/assets/remark/css/bootstrap-extend.css',
	    'resources/assets/remark/css/site.css',

	    //fonts
	    'resources/assets/remark/font/web-icons/web-icons.min.css',

	    //plugins
	    'resources/assets/remark/vendor/animsition/animsition.css',
	    'resources/assets/remark/vendor/asscrollable/asScrollable.css',
	    // 'resources/assets/remark/vendor/switchery/switchery.css',
	    // 'resources/assets/remark/vendor/intro-js/introjs.css',
	    'resources/assets/remark/vendor/slidepanel/slidePanel.css',
	    // 'resources/assets/remark/vendor/flag-icon-css/flag-icon.css'
	], 'public/css/all.css')
	.sass('resources/assets/sass/app.scss', 'public/css');
