<?php

use App\Media;
use Illuminate\Support\Collection;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //print_r(Media::get()->all());

    return view('backend.dashboard');
})->name('dashboard');

Route::resource('attribute', 'AttributeController');

Route::resource('product', 'ProductController');

Route::get('/remark', function () {
    return view('backend.remark');
});

Route::get('/data', function(){

    echo "<pre>";
	$attributeGroups = \App\AttributeGroup::all();
	foreach ($attributeGroups as $attributeGroup) {
		$attributeGroup->setRelation('attributes', $attributeGroup->attributes->keyBy('id'));
	}

    $attributes = new \App\Attribute();
    $attributes = $attributes->without($attributes->excluded_attributes)->keyBy('id');

    print_r($attributeGroups->toArray());

    print_r($attributes->toArray());
});


Route::resource('order', 'OrderController');